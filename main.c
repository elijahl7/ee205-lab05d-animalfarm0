#include "catDatabase.h"
#include "addCats.h"
#include "deleteCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include <stdbool.h>
#include <stdio.h>

int
main ()
{
  puts ("Starting Animal Farm 0");

  initDatabase ();


  addCat ("Loki",   MALE,           PERSIAN,    true,    8.5);
  addCat ("Milo",   MALE,           MANX,       true,    7.0);
  addCat ("Bella",  FEMALE,         MAINE_COON, true,   18.2);
  addCat ("Kali",   FEMALE,         SHORTHAIR,  false,   9.2);
  addCat ("Trin",   FEMALE,         MANX,       true,   12.2);
  addCat ("Chili",  UNKNOWN_GENDER, SHORTHAIR,  false,  19.0);
  printAllCats ();
  int kali = findCat ("Kali");
  updateCatName (kali, "Chili"); // this should fail
  printCat (kali);
  updateCatName (kali, "Capulet");
  updateCatWeight (kali, 9.9);
  fixCat (kali);
  printCat (kali);
  printAllCats ();
  deleteAllCats ();
  printAllCats ();

  puts ("Done with Animal Farm 0");
  return 0;
}
