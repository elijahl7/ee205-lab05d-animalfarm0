#include "reportCats.h"
#include "catDatabase.h"
#include <stdio.h>
#include <string.h>

void
printCat (unsigned long index)
{

  if (index < 0 || index > MAX_CATS)
    {
      printf ("animalFarm0: Bad cat [%lu]\n", index);
      return;
    }

    if (index < numCats) {
        printf("cat index = [%lu]  name=[%s]  gender=[%d]  breed=[%d]  isFixed=[%d]  weight=[%f]\n", index, names[index], genders[index], breeds[index], isFixeds[index], weights[index]);
    } else {
        puts("Cat index out of bounds.");
    }
};

void printAllCats (){
  for (int i = 0; i < numCats; ++i) {
    printf("cat index = [%d]  name=[%s]  gender=[%d]  breed=[%d]  isFixed=[%d]  weight=[%f]\n", i, names[i], genders[i], breeds[i], isFixeds[i], weights[i]);
  }
};

int findCat (char *name){

  if (strlen(name) > MAX_NAME_LENGTH) {
    puts("Cat name is too long");
    return -1;
  }

  for (int i = 0; i < MAX_CATS; ++i) {
      if (strncmp(name, names[i], MAX_NAME_LENGTH) == 0) {
        return i;
      }
  }

  return -1;

};