#include "catDatabase.h"
#include <stdbool.h>
#include <stdio.h>

size_t numCats;

char names[MAX_CATS][MAX_NAME_LENGTH] = {};
enum gender genders[MAX_CATS] = {};
enum breed breeds[MAX_CATS] = {};
bool isFixeds[MAX_CATS] = {};
float weights[MAX_CATS] = {};

size_t
initDatabase ()
{
  numCats = 0;
  return numCats;
}