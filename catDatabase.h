#include <stdio.h>
#include <stdbool.h>
#define MAX_CATS 20
#define MAX_NAME_LENGTH 30

enum gender
{
  UNKNOWN_GENDER,
  MALE,
  FEMALE
};

enum breed
{
  UNKNOWN_BREED,
  MAINE_COON,
  MANX,
  SHORTHAIR,
  PERSIAN,
  SPHYNX
};

extern char names[MAX_CATS][MAX_NAME_LENGTH];
extern enum gender genders[MAX_CATS];
extern enum breed breeds[MAX_CATS];
extern bool isFixeds[MAX_CATS];
extern float weights[MAX_CATS];

extern size_t numCats;

extern size_t initDatabase ();
