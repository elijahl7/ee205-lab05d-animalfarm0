OBJS	= *.o
SOURCE	= *.c
HEADER	= *.h
OUT	= animalFarm0
CC	 = gcc
FLAGS	 = -g -c -Wall

all: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) $(LFLAGS)

*.o: *.c
	$(CC) $(FLAGS) *.c 


clean:
	rm -f $(OBJS)

run:
	clear
	./$(OUT)