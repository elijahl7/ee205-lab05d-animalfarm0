#include "catDatabase.h"
#include "addCats.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

size_t
addCat (char *name, enum gender gender, enum breed breed, bool isFixed,
        float weight)
{ 
  // returns index of the cat in the database
  if (numCats >= MAX_CATS)
    {
      puts ("You have reached the maximum amount of cats!");
      return numCats;
    }

  for (int i = 0; i <= numCats; ++i)
    {
      if (names[i] == name)
        {
          puts ("A cat with this name already exists");
          return numCats;
        }
    }

  if (strlen (name) > MAX_NAME_LENGTH || strlen (name) == 0)
    {
      puts ("The name of a cat must between 1 and 30 characters");
      return numCats;
    }

  strncpy (names[numCats], name, MAX_NAME_LENGTH);
  genders[numCats] = gender;
  breeds[numCats] = breed;
  isFixeds[numCats] = isFixed;
  weights[numCats] = weight;

  return ++numCats;
}