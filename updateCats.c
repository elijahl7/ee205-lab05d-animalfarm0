#include "catDatabase.h"
#include "updateCats.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>


void
fixCat(unsigned long index)
{
  if (index >= numCats)
    {
      puts ("Index out of range");
      return;
    }
  
  isFixeds[index] = true;
}

void
updateCatName (unsigned long index, char *newName)
{
  if (index >= numCats)
    {
      puts ("Index out of range");
      return;
    }
  if (strlen (newName) > MAX_NAME_LENGTH)
    {
      puts ("Name is too long. Must be between 1-30 characters.");
      return;
    }
  if (strlen (newName) == 0)
    {
      puts ("Name must not be empty.");
      return;
    }

  for (int i = 0; i < numCats; ++i)
    {
      if (strncmp (names[i], newName, MAX_NAME_LENGTH) == 0)
        {
          puts ("Cat with this name already exists.");
          return;
        }
    }

  strncpy (names[index], newName, MAX_NAME_LENGTH);
}

void
updateCatWeight (unsigned long index, float newWeight)
{
  if (index >= numCats)
    {
      puts ("Index out of range");
      return;
    }

  weights[index] = newWeight;
}
